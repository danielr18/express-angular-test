// server.js

// BASE
// =============================================================================

// Usamos los paquetes que necesitamos
var express    = require('express');        // Llamamos a Express
var app        = express();                 // Definimos nuestra App usando Express

var port = process.env.PORT || 8000;        // Indicamos el puerto que vamos a utilizar.

// Definimos carpeta para est�ticos o "p�blicos".

app.use(express.static(__dirname + '/public'));

// =============================================================================
app.listen(port);
console.log('Ejecutandose en el puerto: ' + port);